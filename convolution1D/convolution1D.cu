#include <cuda.h>
#include <bits/stdc++.h>

#define BLOCK_SIZE 1024
#define EPSILON 0.1
#define MAX_MASK_SIZE 5

__constant__ float const_mask[MAX_MASK_SIZE];

double Host_time_used;

using namespace std;

bool compare(float *a, float *b, int size){
    for (int i = 0; i < size ; i++){
        if (abs(a[i] - b[i]) > EPSILON)
            return false;
    }
    return true;
}

void Host_convolution_1D(float *N, float *M, float *P, int Mask_Width, int Width){
    for(int i=0;i<Width;i++){
        int N_start_point = i - (Mask_Width/2);
        float Pvalue=0.0;
        for (int j= 0;j<Mask_Width;j++) {
            if (N_start_point+j >= 0 && N_start_point + j < Width) {
                Pvalue+=N[N_start_point+j]*M[j];
            }
        }
        P[i]=Pvalue;
    }
}

__global__ void conv_kernel_basic (float *N, float *mask, float *out, int n_size, int m_size){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    float out_val = 0.0;
    int start = i - (m_size / 2);
    for (int j = 0; j < m_size; j++){
        if (start + j >= 0 && start + j < n_size)
            out_val += N[start + j] * mask[j];
    }
    if(i<n_size)out[i] = out_val;
}

void launch_conv_kernel_basic (float *N, float *mask, float *out, int n_size, int m_size){
    float *d_N;
    float *d_mask;
    float *d_out;
    clock_t start, end;
    double gpu_time;

    start = clock();
    cudaMalloc(&d_N, n_size * sizeof(float));
    cudaMalloc(&d_mask, m_size * sizeof(float));
    cudaMalloc(&d_out, n_size * sizeof(float));

    cudaMemcpy (d_N, N, n_size * sizeof (float), cudaMemcpyHostToDevice);
    cudaMemcpy (d_mask, mask, m_size * sizeof (float), cudaMemcpyHostToDevice);

    dim3 dimGrid (ceil (n_size / float (BLOCK_SIZE)), 1, 1);
    dim3 dimBlock (BLOCK_SIZE, 1, 1);

    conv_kernel_basic<<<dimGrid, dimBlock>>> (d_N, d_mask, d_out, n_size, m_size);
    cudaDeviceSynchronize();

    cudaMemcpy (out, d_out, n_size * sizeof (float), cudaMemcpyDeviceToHost);
    end = clock();
    gpu_time = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("parallel Time Basic: %.10f    Aceleration: %.10f\n", gpu_time,Host_time_used/gpu_time);

    cudaFree (d_N);
    cudaFree (d_mask);
    cudaFree (d_out);
}
void initVector(float *x, int size, float value){
    for(int i=0; i<size; ++i) x[i] = value;
}
void print(float *x, int size){
    for(int i=0;i<size; i++)cout<<x[i]<<" ";
    cout<<endl;
}

__global__ void conv_kernel_constant (float *N, float *out, int n_size, int m_size){
    int i = blockIdx.x * blockDim.x + threadIdx.x;
    float out_val = 0.0;
    int start = i - (m_size / 2);
    for (int j = 0; j < m_size; j++){
        if (start + j >= 0 && start + j < n_size)
            out_val += N[start + j] * const_mask[j];
    }
    out[i] = out_val;
}

void launch_conv_kernel_constant(float *N, float *mask, float *out, int n_size, int m_size){
    float *d_N;
    float *d_out;
    clock_t start, end;
    double gpu_time;

    start = clock();
    cudaMalloc(&d_N, n_size * sizeof(float));
    cudaMalloc(&d_out, n_size * sizeof(float));

    cudaMemcpy (d_N, N, n_size * sizeof (float), cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol (const_mask, mask, m_size * sizeof (float));

    dim3 dimGrid (ceil (n_size / float (BLOCK_SIZE)), 1, 1);
    dim3 dimBlock (BLOCK_SIZE, 1, 1);
    conv_kernel_constant<<<dimGrid, dimBlock>>> (d_N, d_out, n_size, m_size);

    cudaMemcpy (out, d_out, n_size * sizeof (float), cudaMemcpyDeviceToHost);
    end = clock();
    gpu_time = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("parallel Time Constant: %.10f    Aceleration: %.10f\n", gpu_time,Host_time_used/gpu_time);
    cudaFree (d_N);
    cudaFree (d_out);
}

__global__ void conv_kernel_tiled (float *N, float *out, int n_size, int m_size){
    int pos = blockIdx.x * blockDim.x + threadIdx.x;
    int left_pos = (blockIdx.x - 1) * blockDim.x + threadIdx.x;
    int right_pos = (blockIdx.x + 1) * blockDim.x + threadIdx.x;
    int offset = m_size / 2;
    float sum;

    __shared__ float s_vec[BLOCK_SIZE + MAX_MASK_SIZE -1];

    if (threadIdx.x >= blockDim.x - offset){
        if (left_pos < 0)
            s_vec[threadIdx.x - (blockDim.x - offset)] = 0;
        else
            s_vec[threadIdx.x - (blockDim.x - offset)] = N[left_pos];
    }

    (pos < n_size)? s_vec[threadIdx.x + offset] = N[pos] : s_vec[threadIdx.x + offset] = 0;

    if (threadIdx.x < offset){
        if (right_pos >= n_size)
            s_vec[threadIdx.x + blockDim.x + offset] = 0;
        else
            s_vec[threadIdx.x + blockDim.x + offset] = N[right_pos];
    }

    __syncthreads();

    for (int i = 0; i < m_size; i++)
        sum += const_mask[i] * s_vec[threadIdx.x + i];

    out[pos] = sum;

}

void launch_conv_kernel_tiled(float *N, float *mask, float *out, int n_size, int m_size){
    float *d_N;
    float *d_out;
    clock_t start, end;
    double gpu_time;

    start = clock();
    cudaMalloc(&d_N, n_size * sizeof(float));
    cudaMalloc(&d_out, n_size * sizeof(float));

    cudaMemcpy (d_N, N, n_size * sizeof (float), cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol (const_mask, mask, m_size * sizeof (float));

    dim3 dimGrid (ceil (n_size / float (BLOCK_SIZE)), 1, 1);
    dim3 dimBlock (BLOCK_SIZE, 1, 1);
    conv_kernel_tiled<<<dimGrid, dimBlock>>> (d_N, d_out, n_size, m_size);

    cudaMemcpy (out, d_out, n_size * sizeof (float), cudaMemcpyDeviceToHost);
    end = clock();
    gpu_time = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("parallel Time Tiled: %.10f    Aceleration: %.10f\n", gpu_time,Host_time_used/gpu_time);
    cudaFree (d_N);
    cudaFree (d_out);
}

int main(){
    int n_size = 12;    //size of input vector
    int m_size = 5;     //size of mask
    int bytesN = (n_size)*sizeof(float);
    int bytesM = (m_size)*sizeof(float);

    float *N = (float*)malloc(bytesN);
    float *mask = (float*)malloc(bytesM);
    float *h_out = (float*)malloc(bytesN);
    float *basic = (float*)malloc(bytesN);
    float *cons = (float*)malloc(bytesN);
    float *tiled = (float*)malloc(bytesN);

    clock_t start, end;

    initVector(N, n_size, 1);
    initVector(mask, m_size, 1);

    ////////   Secuencial Algorithm   ////////
    start = clock();
    Host_convolution_1D(N, mask, h_out, m_size, n_size);
    end  = clock();
    Host_time_used =  ((double)(end - start)) /CLOCKS_PER_SEC;
    printf("Secuencial time: %.10f\n",Host_time_used);
    /////////////////////////////////////////

    launch_conv_kernel_basic(N, mask, basic, n_size, m_size);

    if (!compare(h_out, basic, n_size))
        cout << "Mal Calculo en Basic Kernel" << endl;
    print(basic,n_size);

    launch_conv_kernel_constant(N, mask, cons, n_size, m_size);

    if (!compare(h_out, cons, n_size))
        cout << "Mal Cálculo en const_Kernel " << endl;

    launch_conv_kernel_tiled(N, mask, tiled, n_size, m_size);

    if (!compare(h_out, tiled, n_size))
        cout << "Mal Calculo en Tiled_Kernel" << endl;

    return 0;
}
