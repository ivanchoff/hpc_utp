Convolución en 1 Dimensión
==========================

Versiones
---------

* Secuencial
* GPU kernel Básico
* GPU kernel con memoria constante
* GPU Kernel con Tiled (memoria compartida)

las diferentes versiones de convolución se probaron con el siguiente conjunto de datos

* 5000
* 500000
* 1000000
* 10000000
* 40000000
* 70000000
* 100000000
* 200000000

Gráfica Tiempos de ejecución.
------------------------------

![](https://github.com/ivanchoff/HPC_UTP/blob/master/convolution1D/img/ejecutionTimes.png "Gráfica de ejecución")

Gráfica Aceleración.
------------------------------
![](https://github.com/ivanchoff/HPC_UTP/blob/master/convolution1D/img/aceleration.png "Gráfica aceleración")
