b/*
 * Sum elements vector
 * ivanchoff@gmail.com
 */

#include<cuda.h>
#include<stdio.h>
#include<time.h>
#include<fstream>
#include<stdlib.h>

#define BLOCK_SIZE 1024

__global__ void reduce(int *g_idata, int *g_odata, int size_vector) {
    __shared__ int sdata[BLOCK_SIZE];
    // each thread loads one element from global to shared mem
    unsigned int tid = threadIdx.x;
    unsigned int i = blockIdx.x*blockDim.x + threadIdx.x;
    if(i<size_vector)
       sdata[tid] = g_idata[i];
    else
      sdata[tid] = 0;
    __syncthreads();
    // do reduction in shared mem
    for(unsigned int s=blockDim.x/2; s > 0; s >>= 1) {
        if(tid < s){
            sdata[tid] += sdata[tid + s];
        }
        __syncthreads();
    }
    // write result for this block to global mem
    if (tid == 0) g_odata[blockIdx.x] = sdata[0];
}

void vector_sum(int *data, int *out, int size){
    for(int i=0; i<size; ++i) out[0] += data[i];
}
int testValues(int A, int B){
    if(A!=B){
        printf("Mal Cálculo...\n"); return 0;
    }
    printf("Buen Cálculo ...\n"); return 0;
}
void initVector(int *x, int size, int value){
    for(int i=0; i < size; ++i) x[i] = value;
}
void printVector(int *x, int size){
    for(int j = 0; j < size; ++j) printf(" %i |",x[j]);
    printf("\n");
}

int main(){
    int *h_M,*d_M,*d_out,*h_out,*h_out_d;
    int size_vector = 10000000;
    cudaError_t error = cudaSuccess;
    clock_t start, end, startGPU, endGPU;
    double cpu_time_used, gpu_time_used;

    int  sizeM;
    sizeM = size_vector*sizeof(int);
    h_M = (int*)malloc(sizeM);
    h_out = (int*)malloc(sizeM);
    h_out_d = (int*)malloc(sizeM);

    initVector(h_M, size_vector, 1);
    initVector(h_out, size_vector, 0);
    initVector(h_out_d, size_vector, 0);

    /////////Algoritmo Secuencial////////////////////////////////////////////
    start = clock();
    vector_sum(h_M, h_out, size_vector);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Tiempo algoritmo secuencial: %.10f\n", cpu_time_used);
    //printVector(h_out, size_vector);
    //////////////////////////////////////////////////////

    error = cudaMalloc((void**)&d_M,sizeM);
    if(error != cudaSuccess){
        printf("Error reservando memoria para d_M");
        exit(0);
    }

    error = cudaMalloc((void**)&d_out,sizeM);
    if(error != cudaSuccess){
        printf("Error reservando memoria para d_M");
        exit(0);
    }

    //////////////////////Algoritmo Paralelo///////////////////////////
    startGPU = clock();
    error = cudaMemcpy(d_M, h_M, sizeM, cudaMemcpyHostToDevice);
    if(error != cudaSuccess){
        printf("Error copiando datos a d_M"); exit(0);
    }
    error = cudaMemcpy(d_out, h_out_d, sizeM, cudaMemcpyHostToDevice);
    if(error != cudaSuccess){
        printf("Error copiando datos a d_M"); exit(0);
    }
    int tmp=size_vector;
    while(tmp>1){
        dim3 dimBlock(BLOCK_SIZE);
        dim3 dimGrid(ceil(tmp/(float)BLOCK_SIZE),1,1);
        reduce<<<dimGrid,dimBlock>>>(d_M,d_out,tmp);
        cudaDeviceSynchronize();
        cudaMemcpy(d_M,d_out,sizeM,cudaMemcpyDeviceToDevice);
        tmp= ceil(tmp/(float)BLOCK_SIZE);
    }
    cudaMemcpy(h_out_d,d_out,sizeM,cudaMemcpyDeviceToHost);
    endGPU = clock();
    gpu_time_used = ((double) (endGPU - startGPU)) / CLOCKS_PER_SEC;
    printf("Tiempo algoritmo paralelo: %.10f\n", gpu_time_used);
    printf("La aceleración obtenida es de %.10fX\n",cpu_time_used/gpu_time_used);

    ///////////////////////Algoritmo Paralelo////////////////////////////
    //printVector(h_out, size_vector);
    testValues(h_out[0], h_out_d[0]);
    printf("Host: %i  GPU: %i",h_out[0],h_out_d[0]);
    free(h_out);
    free(h_M);
    free(h_out_d);
    cudaFree(d_M);
    cudaFree(d_out);
    return 0;
}
