#include<cuda.h>
#include<stdio.h>
#include<time.h>
#include<fstream>
#include<stdlib.h>

#define TILE_WIDTH 2


__global__ void matrixMulKernelTiled(int *d_M, int *d_N, int *d_P, int colM , int colN){
    __shared__ int Mds[TILE_WIDTH][TILE_WIDTH];
    __shared__ int Nds[TILE_WIDTH][TILE_WIDTH];

    int bx = blockIdx.x;
    int by = blockIdx.y;
    int tx = threadIdx.x;
    int ty = threadIdx.y;

    int row = by * TILE_WIDTH + ty;
    int col = bx * TILE_WIDTH + tx;

    float Pvalue = 0;

    for(int m = 0; m < colM / TILE_WIDTH; ++m){
        Mds[ty][tx] = d_M[row*colM + m*TILE_WIDTH + tx];
        Nds[ty][tx] = d_N[(m*TILE_WIDTH + ty) * colN + col];
        __syncthreads();

        for(int k = 0; k < TILE_WIDTH; ++k){
            Pvalue += Mds[ty][k] * Nds[k][tx];
            Mds[ty][k] = 0;
            Nds[k][tx] = 0;
        }
        __syncthreads();
    }
    d_P[row*colN+col] = Pvalue;
}

__global__ void matrixMulKernel(int *d_M, int *d_N, int *d_P, int rowM, int colM, int colN){
    int row = blockIdx.y*blockDim.y+threadIdx.y;
    int col = blockIdx.x*blockDim.x+threadIdx.x;

    int Pvalue;
    if((row < rowM) && (col < colN )){
        Pvalue = 0;
        for (int k = 0; k < colM ; ++k){
            Pvalue += d_M[row*colM+k] * d_N[k*colN+col];
        }
        d_P[row*colN+col] = Pvalue;
    }
}

int matrixMulHost(int *h_M, int *h_N, int *h_P, int rowM, int colM, int colN){
    int Pvalue;
    for(int row = 0; row < rowM ; ++row){
        for(int col = 0; col < colN ; ++col){
            Pvalue = 0;
            for(int k = 0; k < colM ; ++k){
                Pvalue += h_M[row*colM+k] * h_N[k*colN+col];
            }
            h_P[row*colN+col] = Pvalue;
        }
    }
    return 0;
}

int initValues(int *data, int width){
    for(int i = 0; i < width*width; i++)
        data[i] = 2;
    return 0;
}

int printData(int *data, int width){
    for(int i = 0; i < width; ++i){
        for(int j = 0; j < width; ++j){
            printf("%d ", data[(i*width)+j]);
        }
        printf("\n");
    }
    return 0;
}

int testValues(int *A, int *B, int row, int col){
    for(int i = 0; i < row; ++i){
        for(int j = 0; j < col; ++j){
            if(A[(i*col)+j]!=B[(i*col)+j]){
                printf("Mal Cálculo...\n");
                return 0;
            }
        }
    }
    printf("Buen Cálculo ...\n");
    return 0;
}

void initMatrix(int *x, int row, int col, int value){
    for(int i=0; i<row*col; ++i)x[i]=value;
}
void printMatrix(int *x, int row, int col){
    for(int i = 0; i < row ; ++i){
        for(int j = 0; j < col; ++j){
            printf("| %i ",x[(i*col)+j]);
        }
        printf("\n");
    }
}

int main(){
    int *h_M, *h_N, *h_P,*h_P_d;
    int *d_M, *d_N,*d_P;
    //int width = 2048;
    cudaError_t error = cudaSuccess;
    //int size = width * width * sizeof(int);
    clock_t start, end, startGPU, endGPU;
    double cpu_time_used, gpu_time_used;

    int rowM=1024, colM=512, colN=1024, sizeM, sizeN, sizeP;
    sizeM = rowM*colM*sizeof(int);
    sizeN = colM*colN*sizeof(int);
    sizeP = rowM*colN*sizeof(int);
    h_M = (int*)malloc(sizeM);
    h_N = (int*)malloc(sizeN);
    h_P = (int*)malloc(sizeP);
    h_P_d = (int*)malloc(sizeP);

    initMatrix(h_M, rowM, colM, 1);
    initMatrix(h_N, colM, colN, 1);
    initMatrix(h_P, rowM, colN, 0);
    initMatrix(h_P_d, rowM, colN, 0);


    if(h_P_d == NULL)
        return 0;

    //initValues(h_M, width);
    //initValues(h_N, width);

    /////////Algnnoritmo Secuencial////////////////////////////////////////////
    start = clock();
    matrixMulHost(h_M, h_N, h_P, rowM, colM, colN);
    //matrixMulHost(h_M, h_N, h_P, width);
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Tiempo algoritmo secuencial: %.10f\n", cpu_time_used);
    //printMatrix(h_P, rowM, colN);
    //////////////////////////////////////////////////////

    error = cudaMalloc((void**)&d_M,sizeM);
    if(error != cudaSuccess){
        printf("Error reservando memoria para d_M");
        exit(0);
    }

    error = cudaMalloc((void**)&d_N,sizeN);
    if(error != cudaSuccess){
        printf("Error reservando memoria para d_N");
        exit(0);
    }

    error = cudaMalloc((void**)&d_P,sizeP);
    if(error != cudaSuccess){
        printf("Error reservando memoria para d_P");
        exit(0);
    }

    //////////////////////Algoritmo Paralelo///////////////////////////
    startGPU = clock();
    error = cudaMemcpy(d_M, h_M, sizeM, cudaMemcpyHostToDevice);
    if(error != cudaSuccess){
        printf("Error copiando datos a d_M");
        exit(0);
    }

    error = cudaMemcpy(d_N, h_N, sizeN, cudaMemcpyHostToDevice);
    if(error != cudaSuccess){
        printf("Error copiando datos a d_N");
        exit(0);
    }

    int blockSize = TILE_WIDTH;
    dim3 dimBlock(blockSize,blockSize,1);
    dim3 dimGrid(ceil(colN/float(blockSize)),ceil(rowM/float(blockSize)),1);
    //matrixMulKernel<<<dimGrid,dimBlock>>>(d_M,d_N,d_P,rowM, colM, colN);

    matrixMulKernelTiled<<<dimGrid,dimBlock>>>(d_M,d_N,d_P, colM, colN);
    cudaDeviceSynchronize();
    cudaMemcpy(h_P_d,d_P,sizeP,cudaMemcpyDeviceToHost);
    endGPU = clock();
    gpu_time_used = ((double) (endGPU - startGPU)) / CLOCKS_PER_SEC;
    printf("Tiempo algoritmo paralelo: %.10f\n", gpu_time_used);
    printf("La aceleración obtenida es de %.10fX\n",cpu_time_used/gpu_time_used);
    ///////////////////////Algoritmo Paralelo////////////////////////////
    //printMatrix(h_P_d, rowM, colN);
    testValues(h_P_d,h_P,rowM,colN);

    free(h_M);
    free(h_N);
    free(h_P);
    cudaFree(d_M);
    cudaFree(d_N);
    cudaFree(d_P);

    return 0;
}
