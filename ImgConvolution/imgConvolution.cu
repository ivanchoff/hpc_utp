#include <cv.h>
#include <highgui.h>
#include <cuda.h>
#include <bits/stdc++.h>
#define RED 2
#define GREEN 1
#define BLUE 0
#define MASK_WIDTH 3
#define TILE_SIZE 32

typedef unsigned int ui;
typedef unsigned char uc;

__constant__ char M[MASK_WIDTH*MASK_WIDTH];
double cpu_time_used;

using namespace cv;
using namespace std;

__device__ uc clamp(int value){
    if(value < 0)
        value = 0;
    else
        if(value > 255)
            value = 255;
    return (uc)value;
}


__global__ void sobelFilterGlobal(uc *imageInput, int width, int height, ui maskWidth,\
                                  char *Mask,uc *imageOutput){
    ui row = blockIdx.y*blockDim.y+threadIdx.y;
    ui col = blockIdx.x*blockDim.x+threadIdx.x;

    int Pvalue = 0;

    int N_start_point_row = row - (maskWidth/2);
    int N_start_point_col = col - (maskWidth/2);

    for(int i = 0; i < maskWidth; i++){
        for(int j = 0; j < maskWidth; j++ ){
            if((N_start_point_col + j >=0 && N_start_point_col + j < width) \
                    &&(N_start_point_row + i >=0 && N_start_point_row + i < height)){
                Pvalue += imageInput[(N_start_point_row + i)*width+(N_start_point_col + j)] * Mask[i*maskWidth+j];
            }
        }
    }
    if(row*width+col<(width*height))
        imageOutput[row*width+col] = clamp(Pvalue);
}

__global__ void sobelFilterCache(uc *imageInput, int width, int height, \
                            ui maskWidth,uc *imageOutput){

    ui row = blockIdx.y*blockDim.y+threadIdx.y;
    ui col = blockIdx.x*blockDim.x+threadIdx.x;

    int Pvalue = 0;

    int N_start_point_row = row - (maskWidth/2);
    int N_start_point_col = col - (maskWidth/2);

    for(int i = 0; i < maskWidth; i++){
        for(int j = 0; j < maskWidth; j++ ){
            if((N_start_point_col + j >=0 && N_start_point_col + j < width) \
               &&(N_start_point_row + i >=0 && N_start_point_row + i < height)){
                Pvalue += imageInput[(N_start_point_row + i)*width+(N_start_point_col + j)] * M[i*maskWidth+j];
            }
        }
    }
    if(row*width+col < (width*height))
        imageOutput[row*width+col] = clamp(Pvalue);
}

__global__ void sobelFilterShareMem(uc *imageInput, int width, int height, \
        ui maskWidth,uc *imageOutput){
    __shared__ float N_ds[TILE_SIZE + MASK_WIDTH - 1][TILE_SIZE+ MASK_WIDTH - 1];

    int n = maskWidth/2;
    int dest = threadIdx.y*TILE_SIZE+threadIdx.x;
    int destY = dest / (TILE_SIZE+MASK_WIDTH-1);
    int destX = dest % (TILE_SIZE+MASK_WIDTH-1);
    int srcY = blockIdx.y * TILE_SIZE + destY - n;
    int srcX = blockIdx.x * TILE_SIZE + destX - n;
    int src = (srcY * width + srcX);

    if (srcY >= 0 && srcY < height && srcX >= 0 && srcX < width)
        N_ds[destY][destX] = imageInput[src];
    else
        N_ds[destY][destX] = 0;

    // Second batch loading
    dest = threadIdx.y * TILE_SIZE + threadIdx.x + TILE_SIZE * TILE_SIZE;
    destY = dest /(TILE_SIZE + MASK_WIDTH - 1), destX = dest % (TILE_SIZE + MASK_WIDTH - 1);
    srcY = blockIdx.y * TILE_SIZE + destY - n;
    srcX = blockIdx.x * TILE_SIZE + destX - n;
    src = (srcY * width + srcX);
    if (destY < TILE_SIZE + MASK_WIDTH - 1) {
        if (srcY >= 0 && srcY < height && srcX >= 0 && srcX < width)
            N_ds[destY][destX] = imageInput[src];
        else
            N_ds[destY][destX] = 0;
    }
    __syncthreads();

    int accum = 0;
    int y, x;
    for (y = 0; y < maskWidth; y++)
        for (x = 0; x < maskWidth; x++)
            accum += N_ds[threadIdx.y + y][threadIdx.x + x] * M[y * maskWidth + x];
    y = blockIdx.y * TILE_SIZE + threadIdx.y;
    x = blockIdx.x * TILE_SIZE + threadIdx.x;
    if (y < height && x < width)
        imageOutput[(y * width + x)] = clamp(accum);
    __syncthreads();
}

void launchGlobalConv(uc *dataRawImage, char h_M[9], int width, int height, int bytesImg, int bytesM){
    clock_t startGPU, endGPU;
    double gpu_time_used;
    uc *d_dataRawImage,*d_sobelOut, *h_imgOut;
    char *d_M;
    h_imgOut=(uc*)malloc(bytesImg);

    cudaMalloc((void**)&d_dataRawImage,bytesImg);
    cudaMalloc((void**)&d_M,bytesM);
    cudaMalloc((void**)&d_sobelOut,bytesImg);

    startGPU = clock();
    cudaMemcpy(d_dataRawImage,dataRawImage,bytesImg, cudaMemcpyHostToDevice);
    cudaMemcpy(d_M,h_M,bytesM, cudaMemcpyHostToDevice);

    int blockSize = 32;
    dim3 dimBlock(blockSize,blockSize,1);
    dim3 dimGrid(ceil(width/float(blockSize)),ceil(height/float(blockSize)),1);
    sobelFilterGlobal<<<dimGrid,dimBlock>>>(d_dataRawImage,width,height,3,d_M,d_sobelOut);
    cudaDeviceSynchronize();
    cudaMemcpy(h_imgOut,d_sobelOut,bytesImg,cudaMemcpyDeviceToHost);
    endGPU = clock();

    Mat gray_image;
    gray_image.create(height,width,CV_8UC1);
    gray_image.data = h_imgOut;
    imwrite("./outputs/1088278266.png",gray_image);

    gpu_time_used = ((double) (endGPU - startGPU)) / CLOCKS_PER_SEC;
    printf("%.10f  %.10f ", gpu_time_used, cpu_time_used/gpu_time_used);

    cudaFree(d_dataRawImage);
    cudaFree(d_M);
    cudaFree(d_sobelOut);
}

void launchCacheConv(uc *dataRawImage, char h_M[9], int width, int height, int bytesImg, int bytesM){
    clock_t startGPU, endGPU;
    double gpu_time_used;
    uc *d_dataRawImage,*d_sobelOut, *h_imgOut;

    h_imgOut = (uc*)malloc(bytesImg);
    cudaMalloc((void**)&d_dataRawImage,bytesImg);
    cudaMalloc((void**)&d_sobelOut,bytesImg);

    startGPU = clock();
    cudaMemcpy(d_dataRawImage,dataRawImage,bytesImg, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(M,h_M,bytesM);

    //cout<<"with:"<<width<<" height:"<<height<<" bytesImg:"<<bytesImg<<" bytesM:"<<bytesM<<endl;
    int blockSize = 32;
    dim3 dimBlock(blockSize,blockSize,1);
    dim3 dimGrid(ceil(width/float(blockSize)),ceil(height/float(blockSize)),1);
    sobelFilterCache<<<dimGrid,dimBlock>>>(d_dataRawImage,width,height,3,d_sobelOut);
    cudaDeviceSynchronize();
    cudaMemcpy(h_imgOut,d_sobelOut,bytesImg,cudaMemcpyDeviceToHost);
    endGPU = clock();

    Mat gray_image;
    gray_image.create(height,width,CV_8UC1);
    gray_image.data = h_imgOut;
    imwrite("./outputs/1088278266.png",gray_image);

    gpu_time_used = ((double) (endGPU - startGPU)) / CLOCKS_PER_SEC;
    printf("%.10f %.10f ", gpu_time_used, cpu_time_used/gpu_time_used);

    cudaFree(d_dataRawImage);
    cudaFree(d_sobelOut);

}

void launchShareConv(uc *dataRawImage, char h_M[9], int width, int height, int bytesImg, int bytesM){
    clock_t startGPU, endGPU;
    double gpu_time_used;
    uc *d_dataRawImage, *d_sobelOut, *h_imgOut;

    h_imgOut = (uc*)malloc(bytesImg);
    cudaMalloc((void**)&d_dataRawImage,bytesImg);
    cudaMalloc((void**)&d_sobelOut,bytesImg);

    startGPU = clock();
    cudaMemcpy(d_dataRawImage,dataRawImage,bytesImg, cudaMemcpyHostToDevice);
    cudaMemcpyToSymbol(M,h_M,bytesM);
    int blockSize = TILE_SIZE;
    dim3 dimBlock(blockSize,blockSize,1);
    dim3 dimGrid(ceil(width/float(blockSize)),ceil(height/float(blockSize)),1);
    sobelFilterShareMem<<<dimGrid,dimBlock>>>(d_dataRawImage,width,height,MASK_WIDTH,d_sobelOut);
    cudaDeviceSynchronize();
    cudaMemcpy(h_imgOut,d_sobelOut,bytesImg,cudaMemcpyDeviceToHost);
    endGPU = clock();

    Mat gray_image;
    gray_image.create(height,width,CV_8UC1);
    gray_image.data = h_imgOut;
    imwrite("./outputs/1088278266.png",gray_image);

    gpu_time_used = ((double) (endGPU - startGPU)) / CLOCKS_PER_SEC;
    printf("%.10f %.10f\n", gpu_time_used, cpu_time_used/gpu_time_used);

    cudaFree(d_dataRawImage);
    cudaFree(d_sobelOut);
}

int main(int argc, char **argv){
    clock_t start, end;
    char h_M[] = {-1,0,1,-2,0,2,-1,0,1};
    int img = 0;
    string name = "./inputs/img1.jpg";
    //few images
    while(img++<1){
        name[12] = '0'+img;
        uc *dataRawImage;
        Mat image;
        image = imread(name, 0);

        int width = image.size().width;
        int height = image.size().height;
        int bytesImg = sizeof(uc)*width*height;
        int bytesMask = sizeof(char)*9;

        dataRawImage = (uc*)malloc(bytesImg);
        dataRawImage = image.data;

        start = clock();
        Mat grad_x;
        Sobel(image,grad_x,CV_8UC1,1,0,3,1,0,BORDER_DEFAULT);
        imwrite("./outputs/1088278266.png",grad_x);
        end = clock();

        cpu_time_used = ((double) (end - start)) /CLOCKS_PER_SEC;
        printf("host %i: %.10f\n",img,cpu_time_used);

        int cases=0;
        while(cases++<10){ //few tests
            launchGlobalConv(dataRawImage, h_M, width, height, bytesImg, bytesMask);
            launchCacheConv(dataRawImage, h_M, width, height, bytesImg, bytesMask);
            launchShareConv(dataRawImage, h_M, width, height, bytesImg, bytesMask);
        }
    }
    return 0;
}
