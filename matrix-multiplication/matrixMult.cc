/*
 * choff@riseup.net
 */
#include <cuda.h>
#include <bits/stdc++.h>
#include <time.h>

#define WIDTH 1024
#define BLOCK_WIDTH 32.0 //32

using namespace std;
//Sequential matrix-matrix multiplication
void secMatrixMul(float* A, float* B, float* C){
    for(int i=0; i<WIDTH; ++i)
        for(int j=0; j<WIDTH; ++j){
            float sum=0;
            for(int k=0; k<WIDTH; ++k)
                sum += A[i*WIDTH+k] * B[k*WIDTH+j];
            C[i*WIDTH+j] = sum;
        }
}

//print matriz
void print(float x[WIDTH*WIDTH]){
    for(int i=0; i<WIDTH; ++i){
        for(int j=0; j<WIDTH; ++j)
            cout<<x[i*WIDTH+j]<<"|";
        cout<<endl;
    }
}

// kernel for parallelizable solution.
__global__ void matrixMulKernel(float* d_A, float* d_B, float* d_C, int width ){
    //index of d_A and d_C
    int row = blockIdx.y*blockDim.y+threadIdx.y;
    //index of d_B and d_C
    int col = blockIdx.x*blockDim.x+threadIdx.x;
    if((row < width) && (col < width)){
        int c_value = 0;
        for(int k=0; k<width; ++k){
            c_value += d_A[row*width+k]*d_B[k*width+col];
        }
        d_C[row*width+col] = c_value;
    }
}

//prepare and launch kernel
void matrixMul(float* h_A, float* h_B, float* h_C, int n, clock_t &time){
    int size = n * sizeof(float);
    float *d_A, *d_B, *d_C;
    time = clock();
//1 - allocate memory for d_A, d_B, d_C
    cudaMalloc(&d_A,size);
    cudaMalloc(&d_B,size);
    cudaMalloc(&d_C,size);

//2 - Copy data from Host to Device
    cudaMemcpy(d_A, h_A, size,cudaMemcpyHostToDevice);
    cudaMemcpy(d_B, h_B, size,cudaMemcpyHostToDevice);
    float numBlocks = ceil(WIDTH/BLOCK_WIDTH);
    dim3 dimGrid(numBlocks,numBlocks);
    dim3 dimBlock(BLOCK_WIDTH,BLOCK_WIDTH);

//3 - kernel launch code
    matrixMulKernel<<<dimGrid,dimBlock>>>(d_A,d_B,d_C,WIDTH);
//4 - Copy d_C from device to h_C in host
    cudaMemcpy(h_C,d_C,size,cudaMemcpyDeviceToHost);
    time = clock()-time;
    cudaFree(d_A);
    cudaFree(d_B);
    cudaFree(d_C);
}

int validate(float* a, float* b, int n){
    for(int i=0; i<n; ++i){
        if(a[i]!=b[i])return 0;
    }
    return 1;
}

int main(){
    //I/O to read A, B, C
    int n=WIDTH*WIDTH;
    int size = n*sizeof(float);
    float* h_A = (float*)malloc(size);
    float* h_B = (float*)malloc(size);
    float* s_C = (float*)malloc(size);

    for (int i = 0; i < n; ++i){
        h_A[i] = 1;
        h_B[i] = 1;
        s_C[i] = 0;
    }
    float* p_C = (float*)malloc(size);
    clock_t t;
    t=clock();
    secMatrixMul(h_A, h_B, s_C);
    t=clock()-t;
    float a = ((float)t)/CLOCKS_PER_SEC;
    cout<<"Secuencial clock: "<<a<<endl;
    //print(h_C);
    matrixMul(h_A, h_B, p_C, n, t);
    float b=((float)t)/CLOCKS_PER_SEC;
    cout<<"Parallel clock: "<<b<<endl;
    //print(h_C);
    cout<<"Aceleration "<<a/b<<"x"<<endl;
    cout<<((validate(p_C,s_C,n))? "OK": "UPPS")<<endl;
}
